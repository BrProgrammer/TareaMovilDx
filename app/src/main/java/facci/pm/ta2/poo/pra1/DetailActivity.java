package facci.pm.ta2.poo.pra1;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.GetCallback;



public class DetailActivity extends AppCompatActivity {

    TextView precio, descripcion1, Nombre;
    ImageView imagen;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");


        precio = (TextView) findViewById(R.id.img);
        Nombre = (TextView)findViewById(R.id.img3);
        descripcion1 = (TextView)findViewById(R.id.img1);
        imagen = (ImageView)findViewById(R.id.thumbnail);


        // INICIO - CODE6
        //Pregunta 3.1. Accediendo al object_id recibido como parámetro en la actividad

        final DataQuery query = DataQuery.get("item");
        //recibo el parametro
        String parametro = getIntent().getExtras().getString("prueba");
        query.getInBackground(parametro, new GetCallback<DataObject>() {
            @Override
            public void done(DataObject object, DataException e) {
                if (e==null){

                    String prec = (String) object.get("price");
                    String descrip = (String) object.get("description");
                    String nom = (String) object.get("name");
                    Bitmap bitmap = (Bitmap) object.get("image");

                    precio.setText(prec);
                    descripcion1.setText(descrip);
                    Nombre.setText(nom);
                    imagen.setImageBitmap(bitmap);

                }else {
                    //error
                }
            }
        });
       // FIN - CODE6
    }
}
